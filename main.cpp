#include "clara.hpp"

#include <iostream>
#include <stdio.h>
#include <chrono>
#include "Eigen/Dense"
#include <vector>

using namespace clara;
using namespace Eigen;

void printDeviceInfo();
std::tuple<float *, float *, float *> initGPU(float *hA, float *hB, int N);
void matmul(float *dA, float *dB, float *dC, int N);
void getResult(const float *dC, float *hC, int N);

int main(int argc, char **argv) {
  bool print_device_info = false;
  bool show_help = false;
  int matrix_size_N = -1;

  auto cli = Help(show_help) |
    Opt(print_device_info)("Show Device Info")["-i"]["--info"] |
    Arg(matrix_size_N, "MATRIX_SIZE");

  auto result = cli.parse(Args(argc, argv));

  if (!result || show_help) {
    std::cout << cli << "\n";
    return 0;
  } else if (print_device_info) {
    printDeviceInfo();
  }
  if (matrix_size_N == -1) {
    std::cout << "ERROR: Please specify a matrix size\n";
    return 1;
  } else if (matrix_size_N % 16 != 0) {
    std::cout
      << "ERROR: Currently only support matrix sizes with multiple of 16\n";
    return 1;
  }
  printf("Running MATMUL with size=%d\n", matrix_size_N);

  // MatrixXf A(matrix_size_N, matrix_size_N);
  // MatrixXf B(matrix_size_N, matrix_size_N);
  // for(int i=0; i<matrix_size_N; i++) {
  //   for(int j=0; j<matrix_size_N; j++) {
  //     A(i,j) = i;
  //     B(i,j) = i;
  //   }
  // }
  MatrixXf A = MatrixXf::Random(matrix_size_N, matrix_size_N);
  MatrixXf B = MatrixXf::Random(matrix_size_N, matrix_size_N);
  // MatrixXf A = MatrixXf::Ones(matrix_size_N, matrix_size_N);
  // MatrixXf B = MatrixXf::Ones(matrix_size_N, matrix_size_N);

  float *hA = A.data();
  float *hB = B.data();
  auto [dA, dB, dC] = initGPU(hA, hB, matrix_size_N);

  ////////////////////////////////////////////////////////////////////////////////
  // test result:
  ////////////////////////////////////////////////////////////////////////////////
  MatrixXf C_cpu = A * B;
  matmul(dA, dB, dC, matrix_size_N);
  MatrixXf C_gpu = MatrixXf::Zero(matrix_size_N, matrix_size_N);
  getResult(dC, C_gpu.data(), matrix_size_N);
  std::vector<std::pair<int,int>> bad_results;
  for (int i = 0; i < (matrix_size_N); i++) {
    for(int j=0; j<matrix_size_N; j++) {
      if(fabs(C_cpu(i,j) - C_gpu(i,j)) > 1e-3) {
        bad_results.push_back(std::make_pair(i,j));
      }
      if(bad_results.size() > 10) {
        break;
      }
    }
    if(bad_results.size() > 10) {
      break;
    }
  }

  if(bad_results.size() > 0) {
    std::cout << "ERROR: Matrix results not identical!\n";
    for(int i=0;i<bad_results.size();i++) {
      int c_i = bad_results[i].first;
      int c_j = bad_results[i].second;
      std::printf("C(%d,%d)=cpu %e vs gpu %e\n", c_i, c_j, C_cpu(c_i, c_j), C_gpu(c_i, c_j) );
    }
    // std::cout << "Cgpu: \n" << C_gpu << "\n";
    // std::cout << "Ccpu: \n" << C_cpu << "\n";
    return 1;
  }
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////



  ////////////////////////////////////////////////////////////////////////////////
  // Run benchmark
  ////////////////////////////////////////////////////////////////////////////////

  {
    int Ntimes = 20;
    auto t0 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < Ntimes; i++) {
      matmul(dA, dB, dC, matrix_size_N);
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> t = (t1 - t0);
    double tm =
      std::chrono::duration_cast<std::chrono::microseconds>(t).count();
    std::cout << "Time to GPU Multiply A*B (" << Ntimes << " times): " << tm
              << "us\n";
  }
  {
    int Ntimes = 2;
    auto t0 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < Ntimes; i++) {
      MatrixXf C = A * B;
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> t = (t1 - t0);
    double tm =
      std::chrono::duration_cast<std::chrono::microseconds>(t).count();
    std::cout << "Time to CPU Multiply A*B (" << Ntimes << " times): " << tm
              << "us\n";
  }
  return 0;
}
