#include <stdio.h>
#include <stdexcept>
#include <tuple>

#define BLOCKSIZE 16

__global__
void AxB(float* A, float* B, float* C, int N) {
  __shared__ float As[256];
  __shared__ float Bs[256];
  
  for(int loop_block_idx=0; loop_block_idx < (N/BLOCKSIZE); loop_block_idx++) {
    int a_i = blockIdx.y * blockDim.y + threadIdx.y;
    int a_j = loop_block_idx * BLOCKSIZE + threadIdx.x;

    int b_i = loop_block_idx * BLOCKSIZE + threadIdx.y;
    // int b_j = blockIdx.y * blockDim.y + threadIdx.x; // ?
    int b_j = threadIdx.x + blockIdx.x*blockDim.x;

    // column major
    As[threadIdx.y + blockDim.y * threadIdx.x] = A[a_i + a_j * N];
    Bs[threadIdx.y + blockDim.y * threadIdx.x] = B[b_i + b_j * N];

    __syncthreads();

    float c_ij = 0.0;
    for(int i=0; i<BLOCKSIZE; i++) {
      c_ij += As[threadIdx.y + (BLOCKSIZE * i)] * Bs[(threadIdx.x * BLOCKSIZE) + i];
    }

    ////
    // C indices
    int c_i = threadIdx.y + blockIdx.y * blockDim.y;
    int c_j = threadIdx.x + blockIdx.x * blockDim.x;
    // column major
    int c_idx = c_i + c_j * N;
    C[c_idx] += c_ij;

    __syncthreads();
  }

  

}

void printDeviceInfo() {

  int nDevices;

  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n", prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n", prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
           2.0 * prop.memoryClockRate * (prop.memoryBusWidth / 8) / 1.0e6);
  }
}

std::tuple<float *, float *, float *> initGPU(float *hA, float *hB, int N) {
  if (N % 16 != 0) {
    throw std::runtime_error("ERROR: Matrix not a multiple of 16");
  }

  float *dA;
  float *dB;
  float *dC;
  size_t Nb = N * N * sizeof(float);

  // allocate
  cudaMalloc(&dA, Nb);
  cudaMalloc(&dB, Nb);
  cudaMalloc(&dC, Nb);

  // initialize
  cudaMemset(dC, 0, Nb);
  cudaMemcpy(dA, hA, Nb, cudaMemcpyHostToDevice);
  cudaMemcpy(dB, hB, Nb, cudaMemcpyHostToDevice);

  return std::make_tuple(dA, dB, dC);
}

void matmul(float* dA, float* dB, float* dC, int N) {
  // launch kernel
  dim3 grid;
  grid.x = N/BLOCKSIZE;
  grid.y = N/BLOCKSIZE;
  dim3 block;
  block.x = BLOCKSIZE;
  block.y = BLOCKSIZE;
  AxB<<<grid, block>>>(dA, dB, dC, N);
  cudaDeviceSynchronize();
}

void getResult(const float* dC, float* hC, int N) {
  
  cudaMemcpy(hC, dC, N*N*sizeof(float), cudaMemcpyDeviceToHost);
  
}
